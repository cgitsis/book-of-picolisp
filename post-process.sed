#!/bin/sed -f

# Change the title
/<title>.*<\/title>/c\  <title>The Small Book of PicoLisp</title>

# Remove embedded <style>
/<style>/,/<\/style>/d

# Delete some undesired lines
/<h1 class="title">/ d
/<a href="refA.html">/ d
/<span id="sortBtnHome">/ d
/Download/ d

# Remove extra whitespace from Function References
/<dt>Symbol Functions<\/dt>/,/<\/dl>/ {
  s/  \( \)*/  /g
  s/<dd><code>\( \)*/<dd><code>/g
}
/<dt>Forms<\/dt>/,/<\/dl>/ {
  s/  \( \)*/  /g
  s/<dd><code>\( \)*/<dd><code>/g
}

# Try to avoid page breaks inside the largest diagrams
/h2 id="the-picolisp-machine"/,/h2 id="programming-environment"/ s/<pre><code>/<pre><code style="page-break-inside:avoid">/g

# A nasty hack to use CSS class instead of embedded style (which pandoc ignores)
s/\(<p>(c) Software Lab.*\)/<div class=align-right>\1<\/div>/g
s/\(<p>Source.*\)/<div class=align-right>\1<\/div>/g

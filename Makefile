all: small_book_of_picolisp.pdf

small_book_of_picolisp.pdf: stage2.html style.css fonts.css
	weasyprint --stylesheet fonts.css --stylesheet style.css stage2.html small_book_of_picolisp.pdf

stage2.html: stage1.html post-process.sed
	sed -f post-process.sed stage1.html > stage2.html
stage1.html: front-matter.html page-break.html Introduction.html stage0 doc/structures.html
	sh merge.sh
stage0:
	sh download.sh

doc/structures.html: doc/structures code-process.sed
	sed -f code-process.sed doc/structures > doc/structures.html

clean:
	rm -f stage0 stage1.html stage2.html small_book_of_picolisp.pdf
purge: clean
	rm -rf doc

## The Small Book of PicoLisp

A set of scripts to compile and typeset a book containing articles and references from the official documentation of the PicoLisp programming language, found at https://software-lab.de/doc. The objective here is to produce a PDF document optimized for *printing on paper*.

The scripts have been tested on a Debian 11 operating system.

### Dependencies
- `wget` for downloading the HTML source files
- `pandoc` for merging the HTML source files into one
- `weasyprint` for converting the HTML file to PDF [^1]
- `make` for automating the build process
- fonts of your choice (the default configuration uses `fonts-ebgaramond`, `fonts-ebgaramond-extra`, `fonts-inconsolata`)

[^1]: Although version 51 of `weasyprint` (the default in Debian 11) will simply ignore the SVG logo which is supposed to come on the first page. One can install the latest version with the Package Installer for Python `pip`. Instructions at https://weasyprint.org/ .

### Font selection and alternative fonts
Two main fonts are needed to typeset the document: one regular font and one monospace font for the code segments. The selection of fonts can be customised in the file `fonts.css`. Debian offers many packages which contain alternative fonts, such as `fonts-urw-base35`, `fonts-texgyre`, `fonts-sil-gentium`, `fonts-ibm-plex`, `fonts-noto`.

Hints:
- after installing fonts through your package manager, use the command `fc-list` (of fontconfig) to find the exact name of the installed font families.
- the widest code segment in the book seems to be the definition of the function `tell` in the section "Function Reference" of "the PicoLisp Reference". In case you change the code font, you might want to choose the font-family, font-size, @page.margin and pre.margin CSS properties so as to ensure its comments can fit in one line without wrapping.

### Building
After installing the above dependencies and appropriate fonts, run
```
$> make
```
which should generate the file `small_book_of_picolisp.pdf`
